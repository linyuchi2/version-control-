import request from '@/utils/requestLog'

// 添加新建项目
export function addProject(data) {
  return request({
    url: '/hbase/insertData',
    method: 'post',
    data: {
      table: 'projects',
      projectId: data.projectId,
      textarea: data.textarea,
      userId: data.userId,
      createDate: Date.now()
    }
  })
}

// 查看项目
export function queryProject(data) {
  return request({
    url: '/es/queryWithPage',
    method: 'post',
    data: {
      condition: {
        userId: data.userId || '',
        projectId: data.projectId || ''
      },
      order: {
      },
      page: 1,
      size: 1000,
      table: 'projects'
    }
  })
}

// 删除项目
export function deleteProject(data) {
  return request({
    url: '/hbase/deleteByKeyValue',
    method: 'post',
    data: {
      table: 'projects',
      projectId: data.projectId,
      createDate: data.createDate,
      userId: data.userId
    }
  })
}
// 添加新增和删除操作信息
export function addInsertLog(data) {
  return request({
    url: '/hbase/insertData',
    method: 'post',
    data: {
      table: 'journals',
      operationDate: Date.now(),
      data: data.info,
      userId: data.userId
    }
  })
}

// 删除操作信息
export function deleteLog(data) {
  return request({
    url: '/hbase/deleteByKeyValue',
    method: 'post',
    data: {
      table: 'journals',
      userId: data.userId
    }
  })
}

// 查看新增删除操作信息
export function queryLog(data) {
  return request({
    url: '/es/queryWithPage',
    method: 'post',
    data: {
      condition: {
        userId: data.userId
      },
      order: {
        operationDate: 'desc'
      },
      page: data.page,
      size: data.size,
      table: 'journals'
    }
  })
}

// 新建下载量信息
export function addDownloads(data) {
  return request({
    url: '/hbase/insertData',
    method: 'post',
    data: {
      table: 'downloads',
      thisMonth: data.thisMonth,
      lastMonth: data.lastMonth,
      userId: data.userId
    }
  })
}

// 获取下载量信息
export function queryDownloads(data) {
  return request({
    url: '/es/queryWithPage',
    method: 'post',
    data: {
      condition: {
        userId: data.userId
      },
      order: {},
      page: 1,
      size: 10000,
      table: 'downloads'
    }
  })
}

// 修改下载量信息
export function updateDownloads(data) {
  return request({
    url: '/hbase/updateByKeyValue',
    method: 'post',
    data: {
      table: 'downloads',
      condition: {
        userId: data.userId
      },
      thisMonth: data.thisMonth,
      lastMonth: data.lastMonth
    }
  })
}
