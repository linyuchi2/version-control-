import request from '@/utils/requestVersion'
import request2 from '@/utils/requestCos'
export function getversion(data) {
  return request({
    url: '/queryVersion',
    method: 'post',
    data: {
      userId: data.userId,
      projectId: data.projectId || '',
      token: data.token
    }
  })
}

export function addVersion(data) {
  return request({
    url: '/addVersion',
    method: 'post',
    data
  })
}
export function delVersion(data) {
  return request({
    url: '/delVersion',
    method: 'post',
    data
  })
}

export function delKeys(data) {
  return request2({
    url: '/bucket/object',
    method: 'delete',
    data
  })
}

