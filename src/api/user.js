import request from '@/utils/request'

export function login(data) {
  return request({
    url: '/oauth/token',
    method: 'post',
    params: {
      grant_type: 'password',
      username: data.username,
      password: data.password,
      client_id: '1222',
      client_secret: '123456'
    }
  })
}

export function regist(data) {
  return request({
    url: '/user/register',
    method: 'post',
    data: {
      name: data.name,
      userCode: data.userCode,
      username: data.username,
      password: data.password,
      email: data.email,
      phone: data.phone
    }
  })
}
export function getInfo() {
  return request({
    url: '/sys/me',
    method: 'post'
  })
}

export function getPhoneCode(data) {
  return request({
    url: '/sms/dosendMsg',
    method: 'post',
    data: {
      phone: data
    }
  })
}
