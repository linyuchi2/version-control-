13960819008@163.com
采用vue-element-admin后台集成解决方案制作，实现对后端数据渲染，上传版本，控制线上版本发布灰度版本以及对版本的回滚
## Build
```bash

# 安装依赖
npm install

# 启动服务
npm run dev
```

浏览器访问 http://localhost:9528

## 发布

```bash
# 构建测试环境
npm run build:stage

# 构建生产环境
npm run build:prod
```

## 其他

```bash
# 预览发布环境效果
npm run preview

# 预览发布环境效果 + 静态资源分析
npm run preview -- --report

# 代码格式检查
npm run lint

# 代码格式检查并自动修复
npm run lint -- --fix
```